//creation de la classe Warrior
export class Warrior {
    constructor(name, power, life) {
        this.name = name;
        this.power = power;
        this.life = life;
    }//création de la méthode attack
    attack(opponent) {
        // je reduit la vie de l'adversaire du nombre de la puissance du guerrier
        opponent.life -= this.power;
        console.log(`${this.name} attaque ${opponent.name} ! ${opponent.name} perd ${this.power} points de vie, il lui reste ${opponent.life} points de vie`);
    }//création de la méthode isAlive
    isAlive() {
        // je retourne true si le nombre de points de vie est supérieur à 0
        return this.life > 0;
    }
}

//creation des classes WarriorAxe, WarriorSword et WarriorSpear
export class WarriorAxe extends Warrior {
      attack(opponent) {
        if (opponent instanceof WarriorSword) {
            opponent.life -= this.power * 2;
            console.log(`${this.name} attaque ${opponent.name} ! ${opponent.name} perd ${this.power * 2} points de vie, il lui reste ${opponent.life} points de vie`);
        } else {
            //appel de la méthode attack de la classe Warrior
        super.attack(opponent);
      }
}
}

export class WarriorSword extends Warrior {
      attack(opponent){
        //condition pour savoir si l'adversaire est une instance de la classe WarriorAxe
        if (opponent instanceof WarriorSpear) {
            //si c'est le cas, on lui retire le double de la puissance du guerrier
            opponent.life -= this.power * 2;
        } else {
        super.attack(opponent);
      }
   
}
}

export class WarriorSpear extends Warrior {
      attack(opponent){
        if (opponent instanceof WarriorAxe) {
            opponent.life -= this.power * 2;
        } else {
        super.attack(opponent);
      }
   
}
}
