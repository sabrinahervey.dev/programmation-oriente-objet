import {Warrior, WarriorAxe, WarriorSword, WarriorSpear} from './warrior.mjs';


//création des instances guerriers
let Joan = new WarriorSpear('Joan', 10, 100);
let Leon = new WarriorSword('Leon', 8, 120);
let Arthur = new WarriorAxe('Arthur', 12, 80);

//on vérifie les méthodes attack et isAlive
console.log(`Avant l'attaque : ${Joan.name} est vivant : ${Joan.isAlive()}, ${Leon.name} est vivant : ${Leon.isAlive()}`);
/*Joan.attack(Leon);
Leon.attack(Joan);
Arthur.attack(Joan);*/
//Leon.attack(Arthur);
Arthur.attack(Leon);
console.log(`Après l'attaque : ${Joan.name} est vivant : ${Joan.isAlive()}, ${Leon.name} est vivant : ${Leon.isAlive()}`);

//boucle pour que les guerriers s'attaquent jusqu'à ce qu'il n'y en ait plus qu'un de vivant
while (Arthur.isAlive() && Leon.isAlive()) {
    Arthur.attack(Leon);
    Leon.attack(Arthur);
}
//condition pour savoir qui a gagné
if (!Joan.isAlive() && !Arthur.isAlive()) {
    console.log("It's a draw !");
} else if (!Leon.isAlive()) {
    console.log(`${Arthur.name} est vivant !`);
} else {
    console.log(`${Leon.name} est vivant !`);
}
